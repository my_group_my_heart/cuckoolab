/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cuckoo;

/**
 *
 * @author 66986
 */
public class CucHash {
    private int key;
    private String name;
    private CucHash[] table = new CucHash[79];
    private CucHash[] table2 = new CucHash[79];

    public CucHash(int key, String name) {
        this.key = key;
        this.name = name;
        
    }
    public CucHash(){
        
    }

    public int getKey() {
        return key;
    }

    public String getName() {
        return name;
    }
    private int hash1(int key){
        return key % table.length;
    }
    private int hash2(int key){
        return (key / 79) % table2.length;
    }
    public  void inputt(int key, String name){
        if (table[hash1(key)] != null && table[hash1(key)].key == key) {
            table[hash1(key)].key = key;
            table[hash1(key)].name = name;
            return;
        }
        if(table2[hash2(key)] != null && table[hash2(key)].key == key) {
            table2[hash2(key)].key = key;
            table2[hash2(key)].name = name;
            return;
        }
        
        
         int i = 0;
         
         while (true) {
            if (i == 0) { //table 1
                if (table[hash1(key)] == null) {
                    table[hash1(key)] = new CucHash();
                    table[hash1(key)].key = key;
                    table[hash1(key)].name = name;
                    return;
                } else { //table 2 (i==1)
                if (table2[hash2(key)] == null) {
                    table2[hash2(key)] = new CucHash();
                    table2[hash2(key)].key = key;
                    table2[hash2(key)].name = name;
                    return;
                }
            }
            if(i == 0){
                String tempName = table[hash1(key)].name;
                int tempKey = table[hash1(key)].key;
                table[hash1(key)].key = key;
                table[hash1(key)].name = name;
                name = tempName;
                key = tempKey;
            }else{
                String tempValue = table2[hash2(key)].name;
                int tempKey = table2[hash2(key)].key;
                table2[hash2(key)].key = key;
                table2[hash2(key)].name = name;
                key = tempKey;
                name = tempValue;
            }
            i = (i + 1) % 2;
        }
    }
         
         
    }

    public CucHash get(int key) {
        if (table[hash1(key)] != null && table[hash1(key)].key == key) {
            return table[hash1(key)];
        }
        if (table2[hash2(key)] != null && table2[hash2(key)].key == key) {
            return table2[hash2(key)];           
        }
        return null;
    }
    public CucHash delete(int key){
         if (table[hash1(key)] != null && table[hash1(key)].key == key) {
            CucHash temp = table[hash1(key)];
            table[hash1(key)] = null;
             return temp;
         }
          if (table2[hash2(key)] != null && table2[hash2(key)].key == key) {
            CucHash temp = table2[hash2(key)];
            table2[hash2(key)] = null;
            return temp;
        }
         return null;
       
    }
        
    @Override
    public String toString() {
        return "Key: " + key + " Nmae: " + name;
    }
       
    
    
    
    
    
    
}
